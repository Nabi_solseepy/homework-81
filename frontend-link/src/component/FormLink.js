import React, {Component, Fragment} from 'react';
import {Button, Col,  FormGroup, Input,} from "reactstrap";
import {connect} from "react-redux";
import {linkCreate} from "../store/action";

class FormLink extends Component {

    state = {
        originalUrl: ''

    };


    valueLink = (event) => {
            this.setState({[event.target.name]: event.target.value})
    };

    render() {
        return (
            <Fragment>
                <h1>Shorten your link!</h1>
                <div className="container">
                    <FormGroup row>
                        <Col sm={12}>
                            <Input onChange={this.valueLink}  type="text" name="originalUrl" placeholder="Enter URL here" />
                        </Col>
                    </FormGroup>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 1 }}>
                            <Button onClick={() =>  this.props.linkCreate({...this.state})}>Shorten!</Button>
                        </Col>
                    </FormGroup>
                </div>
                <a href={'http://localhost:8000/links/' + this.props.link}>{this.props.link}</a>


            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    link: state.link
});

const mapDispachToProps = dispatch => ({
    linkCreate: (link) => dispatch(linkCreate(link))
});

export default connect(mapStateToProps, mapDispachToProps)(FormLink);