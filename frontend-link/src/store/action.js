import axios from '../axios'


export const CREATE_LINK = 'CREATE_LINK';


export const  createLink = (data) => ({type: CREATE_LINK, data});



export const  linkCreate = (link) => {
    return dispatch => {
        axios.post('/links', link).then(response => {
           dispatch(createLink(response.data.shortUrl))
        })
    }
}