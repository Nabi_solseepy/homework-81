import {CREATE_LINK} from "./action";

const initialState = {
    link : null

};

const reducer = (state = initialState, action) => {
        switch (action.type) {
            case CREATE_LINK:
                return {...state, link: action.data};
            default:
                return state
        }
    };

export default reducer